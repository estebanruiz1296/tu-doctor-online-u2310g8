const especialidadModelo = require("../modelos/EspecialidadModelo");
const especialidadOperaciones = {};

especialidadOperaciones.crearEspecialidad = async (request, response) => {
    try {
        const  objeto = request.body;
        const especialidad = new especialidadModelo(objeto);
        const especialidadGuardada = await especialidad.save();
        response.status(201).send(especialidadGuardada);
    } catch (error) {
        response.status(400).send("Hubo una mala petición, no se ha podido crear el registro.");
        response.status(400).send("Mala Petición" + error);
    }
    
};
especialidadOperaciones.buscarEspecialidades = async (request, response) => {
    try {
        const listaEspecialidades = await especialidadModelo.find();
        if(listaEspecialidades.length > 0){
            response.status(200).send(listaEspecialidades);
        }else{
            response.status(400).send("No hay Especialidades");
        }
    } catch (error) {
        response.status(400).send("Hubo una mala petición, no se ha encontrado los registros.");
    }
};

especialidadOperaciones.modificarEspecialidad = async (request, response) => {
    try {
        let especialidad = await especialidadModelo.updateMany(
            request.params, {$set : request.body}    
        );
        response.status(200).send(especialidad);
    } catch (error) {
        response.status(400).send("Hubo una mala petición, no se ha podido modificar el registro.");
        console.log("Mala petición " + error);
    }
};

especialidadOperaciones.eliminarEspecialiad = async (request, response) => {
    try {
        let eliminarEspecialiad = await especialidadModelo.remove(request.params);
        response.status(200).send(eliminarEspecialiad);
    } catch (error) {
        response.status(400).send("Hubo una mala petición.");
        console.log("Mala petición " + error);
    } 
};

module.exports = especialidadOperaciones;