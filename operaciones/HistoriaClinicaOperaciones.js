const historiaclinicaModelo = require("../modelos/HistoriaClinicaModelo");
const historiaclinicaOperaciones = {};

historiaclinicaOperaciones.crearHistoriaclinica = async (request, response) => {
    try {
        const  objeto = request.body;
        const historiaclinica = new historiaclinicaModelo(objeto);
        const historiaclinicaGuardado = await historiaclinica.save();
        response.status(201).send(historiaclinicaGuardado);
    } catch (error) {
        response.status(400).send("Mala Petición" + error);
    }
};

historiaclinicaOperaciones.buscarHistoriaclinica = async (request, response) =>{
    try {
        const listaHistoriasClinicas = await historiaclinicaModelo.find();
        if(listaHistoriasClinicas.length > 0){
            response.status(200).send(listaHistoriasClinicas);
        }else{
            response.status(400).send("No existen historias clinicas archivadas.");
        }
    } catch (error) {
        response.status(400).send("Hubo una mala petición.");
    }
};

historiaclinicaOperaciones.modificarHistoriaclinica = async (request, response) => {
    try {
        console.log(request.params);
        let historiaclinica = await historiaclinicaModelo.updateMany(
            request.params, 
            {
                $set : request.body
            }    
        );
        response.status(200).send(historiaclinica);
    } catch (error) {
        console.log("Mala petición " + error);
    }
};

module.exports = historiaclinicaOperaciones;