const doctorModelo = require("../modelos/DoctorModelo");
const doctorOperaciones = {};

doctorOperaciones.crearDoctor = async (request, response) => {
    try {
        const  objeto = request.body;
        const doctor = new doctorModelo(objeto);
        const doctorGuardado = await doctor.save();
        response.status(201).send(doctorGuardado);
    } catch (error) {
        response.status(400).send("Mala Petición" + error);
    }
};

doctorOperaciones.buscarDoctores = async (request, response) =>{
    try {
        const listaDoctores = await doctorModelo.find();
        if(listaDoctores.length > 0){
            response.status(200).send(listaDoctores);
        }else{
            response.status(400).send("No hay doctores");
        }
    } catch (error) {
        response.status(400).send("Hubo una mala petición.");
    }
};

doctorOperaciones.buscarDoctor = async (request, response) =>{
    try {
        let idDoctor = request.params; 
        const doctorEncontrado = await doctorModelo.findById(idDoctor);
        response.status(200).send(doctorEncontrado);
    } catch (error) {
        response.status(400).send("Hubo una mala petición, no se ha encontrado el registro: " + error);
    }
};

doctorOperaciones.modificarDoctor = async (request, response) => {
    try {
        console.log(request.params);
        let doctor = await doctorModelo.updateMany(
            request.params, 
            {
                $set : request.body
            }    
        );
        response.status(200).send(doctor);
    } catch (error) {
        console.log("Mala petición " + error);
    }
};

module.exports = doctorOperaciones;