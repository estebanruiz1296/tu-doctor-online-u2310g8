# TuDoctorOnline
## Iniciar Proyecto
- Ejecutar en el terminal de su entorno de trabajo el siguiente comando `npm init` para iniciar la aplicación, configurar con sus preferencias.

## Bibliotecas
> Las siguientes bibliotecas permiten un correcto funcionamiento de la aplicación, en donde se integra el modelo, el enrutamiento, la parte del ORM, la recarga continua y automatica de la aplicación cuando se realizan cambios, también la validación de las cors, es decir que se pueda conectar a los dos protocolos de comunicación: `http` y `https`, a continuación el listado de las bibliotecas empleadas:
- `npm install express`
- `npm install nodemon`
- `npm install mongoose`
- `npm install morgan`
- `npm install cors`

## Iniciar la Aplicación
- `npm start`

## Recomendaciones
- Incluir en el archivo package.json la siguiente clave valor: "start": "nodemon index.js", en scrips, para poder ejecutar el app correctamente.
- Si a la hora de instalar las bibliotecas aparece un error vinculado a SSL ejecutar el siguiente comando en consola: `npm config set strict-ssl false`
- Después de clonar el repo, dirigirse al terminal de su entorno de trabajo y ejecutar el siguiente comando: `npm install` esto con el fin de reinstalar todas las dependencias que le faltan.
