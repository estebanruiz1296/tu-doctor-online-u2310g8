const doctorOperaciones = require("../operaciones/DoctorOperaciones");
const router = require("express").Router();

router.get("/:_id", doctorOperaciones.buscarDoctor);
router.get("/", doctorOperaciones.buscarDoctores);
router.post("/", doctorOperaciones.crearDoctor);
router.put("/:_id", doctorOperaciones.modificarDoctor);

module.exports = router;