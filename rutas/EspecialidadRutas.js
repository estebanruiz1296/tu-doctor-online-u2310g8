const especialidadOperaciones = require("../operaciones/EspecialidadOperaciones")
const router = require("express").Router();

router.get("/", especialidadOperaciones.buscarEspecialidades);
router.post("/", especialidadOperaciones.crearEspecialidad);
router.put("/:_id", especialidadOperaciones.modificarEspecialidad);
router.delete("/:_id", especialidadOperaciones.eliminarEspecialiad);

module.exports = router;