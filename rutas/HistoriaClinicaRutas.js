const historiaclinicaOperaciones = require("../operaciones/HistoriaClinicaOperaciones");
const router = require("express").Router();

router.get("/", historiaclinicaOperaciones.buscarHistoriaclinica);
router.post("/", historiaclinicaOperaciones.crearHistoriaclinica);
router.put("/:_id", historiaclinicaOperaciones.modificarHistoriaclinica);

module.exports = router;